/**
 * A |Collection| based on a JS Array.
 * Properties:
 * - ordered
 * - indexed: every item has an integer key
 * - can hold the same item several times
 * - fast
 */

const { Collection, KeyValueCollection } = require("./api");
const util = require("./util");

function ArrayColl() {
  KeyValueCollection.call(this);
  this._array = [];
}
ArrayColl.prototype = {

  /**
   * Adds this item to the end of the array.
   *
   * You can add them same object several times.
   */
  add : function(item) {
    this._array.push(item);
    this._notifyAdded(item, this);
  },

  _addWithoutObserver : function(item) {
    this._array.push(item);
  },

  /**
   * Removes first instance of this item.
   *
   * If you have added the same object 5 times,
   * you need to call remove() 5 times, or removeEach() once,
   * to remove them all.
   */
  remove : function(item) {
    util.arrayRemove(this._array, item, false);
    this._notifyRemoved(item, this);
  },

  _removeWithoutObserver : function(item) {
    util.arrayRemove(this._array, item, false);
  },

  /**
   * Removes all instances of this item.
   *
   * If you have added the same object 5 times,
   * you need to call remove() 5 times, or removeEach() once,
   * to remove them all.
   */
  removeEach : function(item) {
    while (this.contains(item)) {
      this.remove(item);
    }
  },

  clear : function() {
    for each (let item in this._array)
      this._notifyRemoved(item, this);
    this._array = [];
  },

  get length() {
    return this._array.length;
  },

  contains : function(item) {
    return util.arrayContains(this._array, item);
  },

  // containsKey : defined in KeyValueCollection

  contents : function() {
    return this._array.slice(); // return copy of array
  },

  /**
   * Sets the value at index |i|
   * This is similar to array[i]
   *
   * @param key {Integer}
   */
  set : function(i, item) {
    util.assert(typeof(i) == "number");
    if (this._array.length > i && this._array[i] == item)
      return;
    var oldItem = this._array[i];
    this._array[i] = item;
    if (oldItem !== undefined)
      this._notifyRemoved(oldItem, this);
    if (item !== undefined)
      this._notifyAdded(item, this);
  },

  /**
   * Gets the value at index |i|
   *
   * If the key doesn't exist, returns null.
   * @param key {Integer}
   */
  get : function(i) {
    util.assert(typeof(i) == "number");
    return this._array[i];
  },

  removeKey : function(i) {
    var item = this._array[i];
    if (item == undefined)
      return;
    delete this._array[i];
    this._notifyRemoved(item, this);
  },

  getKeyForValue : function(value) {
    for (let i in this._array) {
      if (this._array[i] == value)
        return i;
    }
    return undefined;
  },

}
util.extend(ArrayColl, KeyValueCollection);

exports.ArrayColl = ArrayColl;
